#include "../../headers/APIRefactoring/APIRefactoringVisitor.h"

#include "../../headers/Identifier.h"

APIRefactoringVisitor::APIRefactoringVisitor(const clang::ASTContext& context, clang::Rewriter& rewriter, const std::unordered_map<std::string, Function>& functions) : RewriterVisitor(context, rewriter), functions(functions) {

}

bool APIRefactoringVisitor::VisitFunctionDecl(clang::FunctionDecl* functionDecl) {
	std::string identifier = getIdentifierFunctionDecl(functionDecl);

	try {
		for (const RemoveAction& removeAction : functions.at(identifier).removeActions) {
			applyRemoveActionFunctionDecl(functionDecl, removeAction);
		}

		for (const InsertAction& insertAction : functions.at(identifier).insertActions) {
			applyInsertActionFunctionDecl(functionDecl, insertAction);
		}

		for (const ReplaceAction& replaceAction : functions.at(identifier).replaceActions) {
			applyReplaceActionFunctionDecl(functionDecl, replaceAction);
		}
	}
	catch (const std::out_of_range& e) {
		return true;
	}

	printFunctionDeclChanges(functionDecl);
	return true;
}

bool APIRefactoringVisitor::VisitCallExpr(clang::CallExpr* callExpr) {
	std::string identifier = getIdentifierCallExpr(callExpr);

	try {
		for (const RemoveAction& removeAction : functions.at(identifier).removeActions) {
			applyRemoveActionCallExpr(callExpr, removeAction);
		}

		for (const InsertAction& insertAction : functions.at(identifier).insertActions) {
			applyInsertActionCallExpr(callExpr, insertAction);
		}

		for (const ReplaceAction& replaceAction : functions.at(identifier).replaceActions) {
			applyReplaceActionCallExpr(callExpr, replaceAction);
		}
	}
	catch (const std::out_of_range& e) {
		return true;
	}

	printCallExprChanges(callExpr);
	return true;
}

std::string APIRefactoringVisitor::getIdentifierFunctionDecl(clang::FunctionDecl* functionDecl) {
	const clang::FunctionDecl* function = functionDecl;

	if (function->isCXXClassMember()) {
		const clang::CXXMethodDecl* methodDecl = clang::dyn_cast<const clang::CXXMethodDecl>(function);

		for (const clang::CXXMethodDecl* overriddenMethodDecl : methodDecl->overridden_methods()) {
			if (overriddenMethodDecl->size_overridden_methods() == 0) {
				function = clang::dyn_cast<const clang::FunctionDecl>(overriddenMethodDecl);
			}
		}
	}

	if (function->isFunctionTemplateSpecialization()) {
		function = function->getTemplateSpecializationInfo()->getTemplate()->getAsFunction();
	}

	std::string name = function->getQualifiedNameAsString();
	std::vector<std::string> arguments;

	for (int i = 0; i < function->getNumParams(); i++) {
		const clang::ParmVarDecl* paramDecl = function->getParamDecl(i);
		clang::SourceLocation startLocation = paramDecl->getTypeSpecStartLoc();
		clang::SourceLocation endLocation = paramDecl->getTypeSpecEndLoc();
		clang::SourceRange sourceRange(startLocation, endLocation);
		clang::CharSourceRange charRange(sourceRange, true);

		arguments.push_back(clang::Lexer::getSourceText(charRange, context.getSourceManager(), context.getLangOpts()).str());
	}

	return identifier(name, arguments);
}

void APIRefactoringVisitor::applyInsertActionFunctionDecl(clang::FunctionDecl* functionDecl, const InsertAction& insertAction) {
	switch (insertAction.target) {
		case Target::Invalid:
			llvm::errs() << "Error: Unknown target parameter" << "\n";
			return;
		case Target::Parameter:
			if (functionDecl->getNumParams() == 0) {
				insertFunctionDeclParam(functionDecl, 0, insertAction.value);
				return;
			}

			for (int i = 0; i < functionDecl->getNumParams(); i++) {
				if (functionDecl->getParamDecl(i)->getNameAsString() == insertAction.reference) {
					switch (insertAction.insertionLocation) {
						case Location::Invalid:
							llvm::errs() << "Error: Unknown location parameter" << "\n";
							return;
						case Location::Before:
							insertFunctionDeclParam(functionDecl, i, insertAction.value);
							return;
						case Location::After:
							insertFunctionDeclParam(functionDecl, i + 1, insertAction.value);
							return;
						default:
							llvm::errs() << "Error: Location parameter '" << insertAction.insertionLocation << "' is invalid for operation insert" << "\n";
							return;
					}
				}
			}

			llvm::outs() << "Warning: Could not find "
						 << insertAction.target << " '" << insertAction.reference << "' "
						 << "of function " << functionDecl->getNameAsString() << " "
						 << "from InsertAction:" << "\n";
			llvm::outs() << nlohmann::json(insertAction).dump(4) << "\n";
			llvm::outs() << "\n";
			return;
		default:
			llvm::errs() << "Error: Target parameter '" << insertAction.target << "' is invalid for operation insert" << "\n";
			return;
	}
}

void APIRefactoringVisitor::applyRemoveActionFunctionDecl(clang::FunctionDecl* functionDecl, const RemoveAction& removeAction) {
	switch (removeAction.target) {
		case Target::Invalid:
			llvm::errs() << "Error: Unknown target parameter" << "\n";
			return;
		case Target::Parameter:
			for (int i = 0; i < functionDecl->getNumParams(); i++) {
				if (functionDecl->getParamDecl(i)->getNameAsString() == removeAction.value) {
					removeFunctionDeclParam(functionDecl, i);
					return;
				}
			}

			llvm::outs() << "Warning: Could not find "
						 << removeAction.target << " '" << removeAction.value << "' "
						 << "of function " << functionDecl->getNameAsString() << " "
						 << "from RemoveAction:" << "\n";
			llvm::outs() << nlohmann::json(removeAction).dump(4) << "\n";
			llvm::outs() << "\n";
			return;
		default:
			llvm::errs() << "Error: Target parameter '" << removeAction.target << "' is invalid for operation remove" << "\n";
			return;
	}
}

void APIRefactoringVisitor::applyReplaceActionFunctionDecl(clang::FunctionDecl* functionDecl, const ReplaceAction& replaceAction) {
	switch (replaceAction.target) {
		case Target::Invalid:
			llvm::errs() << "Error: Unknown target parameter" << "\n";
			return;
		case Target::Function_Name:
			if (functionDecl->getNameAsString() == replaceAction.oldValue) {
				replaceFunctionDeclName(functionDecl, replaceAction.newValue);
				return;
			}

			llvm::outs() << "Warning: Could not find "
						 << replaceAction.target << " '" << replaceAction.oldValue << "' "
						 << "of function " << functionDecl->getNameAsString() << " "
						 << "from ReplaceAction:" << "\n";
			llvm::outs() << nlohmann::json(replaceAction).dump(4) << "\n";
			llvm::outs() << "\n";
			return;
		case Target::Parameter_Name:
			for (int i = 0; i < functionDecl->getNumParams(); i++) {
				if (functionDecl->getParamDecl(i)->getNameAsString() == replaceAction.oldValue) {
					replaceFunctionDeclParamName(functionDecl, i, replaceAction.newValue);
					return;
				}
			}

			llvm::outs() << "Warning: Could not find "
						 << replaceAction.target << " '" << replaceAction.oldValue << "' "
						 << "of function " << functionDecl->getNameAsString() << " "
						 << "from ReplaceAction:" << "\n";
			llvm::outs() << nlohmann::json(replaceAction).dump(4) << "\n";
			llvm::outs() << "\n";
			return;
		case Target::Parameter_Type:
			for (int i = 0; i < functionDecl->getNumParams(); i++) {
				if (functionDecl->getParamDecl(i)->getNameAsString() == replaceAction.oldValue) {
					replaceFunctionDeclParamType(functionDecl, i, replaceAction.newValue);
					return;
				}
			}

			llvm::outs() << "Warning: Could not find "
						 << replaceAction.target << " '" << replaceAction.oldValue << "' "
						 << "of function " << functionDecl->getNameAsString() << " "
						 << "from ReplaceAction:" << "\n";
			llvm::outs() << nlohmann::json(replaceAction).dump(4) << "\n";
			llvm::outs() << "\n";
			return;
		case Target::Return_Type:
			replaceReturnType(functionDecl, replaceAction.newValue);
			return;
		case Target::Function_Qualifier:
			replaceQualifier(functionDecl, replaceAction.newValue);
			return;
		case Target::Function_Call:
			return;
		default:
			llvm::errs() << "Error: Target parameter '" << replaceAction.target << "' is invalid for operation replace" << "\n";
			return;
	}
}

std::string APIRefactoringVisitor::getIdentifierCallExpr(clang::CallExpr* callExpr) {
	return getIdentifierFunctionDecl(callExpr->getDirectCallee());
}

void APIRefactoringVisitor::applyInsertActionCallExpr(clang::CallExpr* callExpr, const InsertAction& insertAction) {
	switch (insertAction.target) {
		case Target::Invalid:
			llvm::errs() << "Error: Unknown target parameter" << "\n";
			return;
		case Target::Parameter:
			if (callExpr->getNumArgs() == 0) {
				insertCallExprParam(callExpr, 0, insertAction.defaultValue);
				return;
			}

			for (int i = 0; i < callExpr->getDirectCallee()->getNumParams(); i++) {
				if (callExpr->getDirectCallee()->getParamDecl(i)->getNameAsString() == insertAction.reference) {
					switch (insertAction.insertionLocation) {
						case Location::Invalid:
							llvm::errs() << "Error: Unknown location parameter" << "\n";
							return;
						case Location::Before:
							insertCallExprParam(callExpr, i, insertAction.defaultValue);
							return;
						case Location::After:
							insertCallExprParam(callExpr, i + 1, insertAction.defaultValue);
							return;
						default:
							llvm::errs() << "Error: Location parameter '" << insertAction.insertionLocation << "' is invalid for operation insert" << "\n";
							return;
					}
				}
			}

			llvm::outs() << "Warning: Could not find "
						 << insertAction.target << " '" << insertAction.reference << "' "
						 << "of function " << callExpr->getDirectCallee()->getNameAsString() << " "
						 << "from InsertAction:" << "\n";
			llvm::outs() << nlohmann::json(insertAction).dump(4) << "\n";
			llvm::outs() << "\n";
			return;
		default:
			llvm::errs() << "Error: Target parameter '" << insertAction.target << "' is invalid for operation insert" << "\n";
			return;
	}
}

void APIRefactoringVisitor::applyRemoveActionCallExpr(clang::CallExpr* callExpr, const RemoveAction& removeAction) {
	switch (removeAction.target) {
		case Target::Invalid:
			llvm::errs() << "Error: Unknown target parameter" << "\n";
			return;
		case Target::Parameter:
			for (int i = 0; i < callExpr->getDirectCallee()->getNumParams(); i++) {
				if (callExpr->getDirectCallee()->getParamDecl(i)->getNameAsString() == removeAction.value) {
					removeCallExprParam(callExpr, i);
					return;
				}
			}

			llvm::outs() << "Warning: Could not find "
						 << removeAction.target << " '" << removeAction.value << "' "
						 << "of function " << callExpr->getDirectCallee()->getNameAsString() << " "
						 << "from RemoveAction:" << "\n";
			llvm::outs() << nlohmann::json(removeAction).dump(4) << "\n";
			llvm::outs() << "\n";
			return;
		default:
			llvm::errs() << "Error: Target parameter '" << removeAction.target << "' is invalid for operation remove" << "\n";
			return;
	}
}

void APIRefactoringVisitor::applyReplaceActionCallExpr(clang::CallExpr* callExpr, const ReplaceAction& replaceAction) {
	switch (replaceAction.target) {
		case Target::Invalid:
			llvm::errs() << "Error: Unknown target parameter" << "\n";
			return;
		case Target::Function_Name:
			if (callExpr->getDirectCallee()->getNameAsString() == replaceAction.oldValue) {
				replaceCallExprName(callExpr, replaceAction.newValue);
				return;
			}

			llvm::outs() << "Warning: Could not find "
						 << replaceAction.target << " '" << replaceAction.oldValue << "' "
						 << "of function " << callExpr->getDirectCallee()->getNameAsString() << " "
						 << "from ReplaceAction:" << "\n";
			llvm::outs() << nlohmann::json(replaceAction).dump(4) << "\n";
			llvm::outs() << "\n";
			return;
		case Target::Parameter_Name:
		case Target::Parameter_Type:
		case Target::Return_Type:
		case Target::Function_Qualifier:
			return;
		case Target::Function_Call:
			if (callExpr->getDirectCallee()->getNameAsString() == replaceAction.oldValue) {
				replaceCallExprName(callExpr, replaceAction.newValue);
				return;
			}

			llvm::outs() << "Warning: Could not find "
						 << replaceAction.target << " '" << replaceAction.oldValue << "' "
						 << "of function " << callExpr->getDirectCallee()->getNameAsString() << " "
						 << "from ReplaceAction:" << "\n";
			llvm::outs() << nlohmann::json(replaceAction).dump(4) << "\n";
			llvm::outs() << "\n";
			return;
		default:
			llvm::errs() << "Error: Target parameter '" << replaceAction.target << "' is invalid for operation replace" << "\n";
			return;
	}
}