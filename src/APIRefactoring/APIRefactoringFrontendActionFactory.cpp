#include <utility>

#include "../../headers/APIRefactoring/APIRefactoringFrontendActionFactory.h"
#include "../../headers/APIRefactoring/APIRefactoringAction.h"

APIRefactoringFrontendActionFactory::APIRefactoringFrontendActionFactory(bool rewriteFiles, const std::unordered_map<std::string, Function>& functions) : RewriterFrontendActionFactory(rewriteFiles), functions(functions) {

}

std::unique_ptr<clang::FrontendAction> APIRefactoringFrontendActionFactory::create() {
	return std::make_unique<APIRefactoringAction>(this->rewriteFiles, functions);
}

std::unique_ptr<clang::tooling::FrontendActionFactory> newAPIRefactoringFrontendActionFactory(bool rewriteFiles, const std::unordered_map<std::string, Function>& functions) {
	return std::unique_ptr<clang::tooling::FrontendActionFactory>(new APIRefactoringFrontendActionFactory(rewriteFiles, functions));
}