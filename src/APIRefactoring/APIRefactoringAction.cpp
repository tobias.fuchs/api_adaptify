#include "../../headers/APIRefactoring/APIRefactoringAction.h"
#include "../../headers/APIRefactoring/APIRefactoringConsumer.h"

APIRefactoringAction::APIRefactoringAction(bool rewriteFiles, const std::unordered_map<std::string, Function>& functions) : RewriterAction(rewriteFiles), functions(functions) {

}

std::unique_ptr<clang::ASTConsumer> APIRefactoringAction::CreateASTConsumer(clang::CompilerInstance& compiler, llvm::StringRef inFile) {
	rewriter.setSourceMgr(compiler.getSourceManager(), compiler.getLangOpts());
	return std::make_unique<APIRefactoringConsumer>(compiler.getASTContext(), rewriter, functions);
}