#include "../headers/Identifier.h"

std::string identifier(const std::string& name, const std::vector<std::string>& arguments) {
	std::string identifier = name;
	identifier += "(";

	for (int i = 0; i < arguments.size(); i++) {
		identifier += arguments[i];

		if (i < arguments.size() - 1) {
			identifier += ",";
		}
	}

	identifier += ")";
	return identifier;
}