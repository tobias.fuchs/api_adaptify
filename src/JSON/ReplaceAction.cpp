#include "../../headers/JSON/ReplaceAction.h"

void to_json(nlohmann::json& j, const ReplaceAction& p) {
	j = nlohmann::json {
			{"target", p.target},
			{"oldValue", p.oldValue},
			{"newValue", p.newValue}
	};
}

void from_json(const nlohmann::json& j, ReplaceAction& p) {
	if (j.contains("target")) {
		j.at("target").get_to(p.target);
	}
	else {
		p.target = Target::Target::Invalid;
	}

	if (j.contains("oldValue")) {
		j.at("oldValue").get_to(p.oldValue);
	}
	else {
		p.oldValue = "";
	}

	if (j.contains("newValue")) {
		j.at("newValue").get_to(p.newValue);
	}
	else {
		p.newValue = "";
	}
}