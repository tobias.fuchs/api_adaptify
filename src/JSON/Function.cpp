#include "../../headers/JSON/Function.h"

void to_json(nlohmann::json& j, const Function& p) {
	j = nlohmann::json {
			{"name", p.name},
			{"arguments", p.arguments},
			{"insertActions", p.insertActions},
			{"removeActions", p.removeActions},
			{"replaceActions", p.replaceActions}
	};
}

void from_json(const nlohmann::json& j, Function& p) {
	j.at("name").get_to(p.name);
	j.at("arguments").get_to(p.arguments);
	j.at("insertActions").get_to(p.insertActions);
	j.at("removeActions").get_to(p.removeActions);
	j.at("replaceActions").get_to(p.replaceActions);
}