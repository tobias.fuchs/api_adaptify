#include "../../headers/JSON/InsertAction.h"

void to_json(nlohmann::json& j, const InsertAction& p) {
	j = nlohmann::json {
			{"target", p.target},
			{"insertionLocation", p.insertionLocation},
			{"reference", p.reference},
			{"value", p.value},
			{"defaultValue", p.defaultValue}
	};
}

void from_json(const nlohmann::json& j, InsertAction& p) {
	if (j.contains("target")) {
		j.at("target").get_to(p.target);
	}
	else {
		p.target = Target::Target::Invalid;
	}

	if (j.contains("insertionLocation")) {
		j.at("insertionLocation").get_to(p.insertionLocation);
	}
	else {
		p.insertionLocation = Location::Location::Invalid;
	}

	if (j.contains("reference")) {
		j.at("reference").get_to(p.reference);
	}
	else {
		p.reference = "";
	}

	if (j.contains("value")) {
		j.at("value").get_to(p.value);
	}
	else {
		p.value = "";
	}

	if (j.contains("defaultValue")) {
		j.at("defaultValue").get_to(p.defaultValue);
	}
	else {
		p.defaultValue = "";
	}
}