#include "../../headers/JSON/Change.h"

void to_json(nlohmann::json& j, const Change& p) {
	j = nlohmann::json {
			{"file", p.file},
			{"functions", p.functions}
	};
}

void from_json(const nlohmann::json& j, Change& p) {
	j.at("file").get_to(p.file);
	j.at("functions").get_to(p.functions);
}