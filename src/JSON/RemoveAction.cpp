#include "../../headers/JSON/RemoveAction.h"

void to_json(nlohmann::json& j, const RemoveAction& p) {
	j = nlohmann::json {
			{"target", p.target},
			{"value", p.value}
	};
}

void from_json(const nlohmann::json& j, RemoveAction& p) {
	if (j.contains("target")) {
		j.at("target").get_to(p.target);
	}
	else {
		p.target = Target::Invalid;
	}

	if (j.contains("value")) {
		j.at("value").get_to(p.value);
	}
	else {
		p.value = "";
	}
}