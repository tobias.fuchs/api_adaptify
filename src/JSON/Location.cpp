#include "../../headers/JSON/Location.h"

void Location::to_json(nlohmann::json& j, const Location& location) {
	switch (location) {
		case Invalid:
			j = "invalid";
			return;
		case Before:
			j = "before";
			return;
		case After:
			j = "after";
			return;
	}
}

void Location::from_json(const nlohmann::json& j, Location& location) {
	if (j == "before") {
		location = Before;
		return;
	}

	if (j == "after") {
		location = After;
		return;
	}

	location = Invalid;
}

llvm::raw_ostream& Location::operator<<(llvm::raw_ostream& os, const Location& location) {
	switch (location) {
		case Before:
			os << "before";
			break;
		case After:
			os << "after";
			break;
		case Invalid:
			break;
	}

	return os;
}