#include "../../headers/JSON/Target.h"

void Target::to_json(nlohmann::json& j, const Target& target) {
	switch (target) {
		case Invalid:
			j = "invalid";
			return;
		case Function_Name:
			j = "function name";
			return;
		case Parameter:
			j = "parameter";
			return;
		case Parameter_Name:
			j = "parameter name";
			return;
		case Parameter_Type:
			j = "parameter type";
			return;
		case Return_Type:
			j = "return type";
			return;
		case Function_Qualifier:
			j = "function qualifier";
			return;
		case Function_Call:
			j = "function call";
			return;
	}
}

void Target::from_json(const nlohmann::json& j, Target& target) {
	if (j == "function name") {
		target = Function_Name;
		return;
	}

	if (j == "parameter") {
		target = Parameter;
		return;
	}

	if (j == "parameter name") {
		target = Parameter_Name;
		return;
	}

	if (j == "parameter type") {
		target = Parameter_Type;
		return;
	}

	if (j == "return type") {
		target = Target::Return_Type;
		return;
	}

	if (j == "function qualifier") {
		target = Target::Function_Qualifier;
		return;
	}

	if (j == "function call") {
		target = Target::Function_Call;
		return;
	}

	target = Target::Invalid;
}

llvm::raw_ostream& Target::operator<<(llvm::raw_ostream& os, const Target& target) {
	switch (target) {
		case Function_Name:
			os << "function name";
			break;
		case Parameter:
			os << "parameter";
			break;
		case Parameter_Name:
			os << "parameter name";
			break;
		case Parameter_Type:
			os << "parameter type";
			break;
		case Return_Type:
			os << "return type";
			break;
		case Function_Qualifier:
			os << "function qualifier";
		case Function_Call:
			os << "function call";
		case Invalid:
			break;
	}

	return os;
}