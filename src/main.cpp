#include <string>
#include <fstream>
#include <filesystem>
#include <unordered_map>

#include <llvm/Support/CommandLine.h>
#include <clang/Tooling/Tooling.h>
#include <clang/Tooling/CommonOptionsParser.h>
#include <nlohmann/json.hpp>

#include "../headers/Identifier.h"
#include "../headers/JSON/Change.h"
#include "../headers/APIRefactoring/APIRefactoringFrontendActionFactory.h"

static llvm::cl::OptionCategory APIRefactoring("APIRefactoring");
llvm::cl::opt<std::string> jsonFile(llvm::cl::Positional, llvm::cl::Required, llvm::cl::desc("<config>"), llvm::cl::cat(APIRefactoring));
llvm::cl::opt<bool> rewriteFiles("rewriteFiles", llvm::cl::desc("Applies all transformations if this flag is set"), llvm::cl::cat(APIRefactoring));

std::unordered_map<std::string, Function> parseJSON(const std::string& file, llvm::ArrayRef<std::string> sourcePathList) {
	nlohmann::json data = nlohmann::json::parse(std::ifstream(file));
	std::vector<Change> changes = data.get<std::vector<Change>>();

	std::unordered_map<std::string, Function> functions;

	for (const std::string& sourcePath : sourcePathList) {
		for (const Change& change : changes) {
			if (std::filesystem::path(sourcePath).filename().string() == change.file) {
				for (const Function& function : change.functions) {
					functions[identifier(function.name, function.arguments)] = function;
				}
			}
		}
	}

	return functions;
}

int main(int argc, const char** argv) {
    llvm::Expected<clang::tooling::CommonOptionsParser> optionsParser = clang::tooling::CommonOptionsParser::create(argc, argv, APIRefactoring);

    if (!optionsParser) {
        llvm::errs() << optionsParser.takeError();
        return 1;
    }

    clang::tooling::ClangTool tool(optionsParser->getCompilations(), optionsParser->getSourcePathList());
	return tool.run(newAPIRefactoringFrontendActionFactory(rewriteFiles, parseJSON(jsonFile, tool.getSourcePaths())).get());
}