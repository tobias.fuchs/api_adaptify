// RUN: api_refactoring changes.json "%s" -- | FileCheck-14 "%s"

// CHECK: :[[@LINE+3]]
// CHECK-NEXT: void overloaded(int a)
// CHECK-NEXT: void overloaded(int a, bool b)
void overloaded(int a) {

}

// CHECK: :[[@LINE+3]]
// CHECK-NEXT: void overloaded(int a, bool b)
// CHECK-NEXT: void overloaded(int a)
void overloaded(int a, bool b) {

}