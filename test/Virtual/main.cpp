// RUN: api_refactoring changes.json "%s" -- | FileCheck-14 "%s"

class Parent {
	// CHECK: :[[@LINE+3]]
	// CHECK-NEXT: virtual int test()
	// CHECK-NEXT: virtual float test()
	virtual int test() const {
		return 0;
	}
};

class Child : Parent {
	// CHECK: :[[@LINE+3]]
	// CHECK-NEXT: int test()
	// CHECK-NEXT: float test()
	int test() const override {
		return 1;
	}
};