// RUN: api_refactoring changes.json "%s" -- | FileCheck-14 "%s"

class Test {
	// CHECK: :[[@LINE+3]]
	// CHECK-NEXT: constexpr Test()
	// CHECK-NEXT: const Test()
	constexpr Test() {

	}

	// CHECK: :[[@LINE+3]]
	// CHECK-NEXT: virtual int foo()
	// CHECK-NEXT: const int foo()
	virtual int foo() {
		// CHECK-NOT: :[[@LINE+1]]
		return foo();
	}

	// CHECK: :[[@LINE+3]]
	// CHECK-NEXT: const volatile unsigned int bar() const
	// CHECK-NEXT: explicit float bar() const
	template <typename T> const volatile unsigned int bar() const;

	// CHECK: :[[@LINE+3]]
	// CHECK-NEXT: template <> const volatile unsigned int bar<int>() const
	// CHECK-NEXT: template <> explicit float bar<int>() const
	template <> const volatile unsigned int bar<int>() const {
		// CHECK-NOT: :[[@LINE+1]]
		return bar<int>();
	}
};