// RUN: api_refactoring changes.json "%s" -- | FileCheck-14 "%s"

// CHECK: :[[@LINE+3]]
// CHECK-NEXT: int main()
// CHECK-NEXT: int test()
int main() {
	// CHECK: :[[@LINE+3]]
	// CHECK-NEXT: main()
	// CHECK-NEXT: test()
	return main();
}