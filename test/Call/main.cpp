// RUN: api_refactoring changes.json "%s" -- | FileCheck-14 "%s"

void foo(int i) {

}

void bar(int j) {

}

int main(int argc, char** argv) {
	// CHECK: :[[@LINE+3]]
	// CHECK-NEXT: foo(argc)
	// CHECK-NEXT: bar(argc)
	foo(argc);
}