// RUN: api_refactoring changes.json "%s" -- | FileCheck-14 "%s"

// CHECK: :[[@LINE+3]]
// CHECK-NEXT: void foo(int bar)
// CHECK-NEXT: void foo()
void foo(int bar) {
	// CHECK: :[[@LINE+3]]
	// CHECK-NEXT: foo(bar)
	// CHECK-NEXT: foo()
	foo(bar);
}

// CHECK: :[[@LINE+3]]
// CHECK-NEXT: const volatile int function(int x, signed int y = 0, unsigned int z = 1)
// CHECK-NEXT: const volatile int function(int y = 0, unsigned int z = 1)
const volatile int function(int x, signed int y = 0, unsigned int z = 1) {
	// CHECK: :[[@LINE+3]]
	// CHECK-NEXT: function(1, 2, 3)
	// CHECK-NEXT: function(2, 3)
	return function(1, 2, 3);
}

// CHECK: :[[@LINE+3]]
// CHECK-NEXT: int main(int argc, char** argv)
// CHECK-NEXT: int main()
int main(int argc, char** argv) {
	// CHECK: :[[@LINE+3]]
	// CHECK-NEXT: main(argc, argv)
	// CHECK-NEXT: main()
	return main(argc, argv);
}