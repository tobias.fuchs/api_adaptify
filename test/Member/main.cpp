// RUN: api_refactoring changes.json "%s" -- | FileCheck-14 "%s"

class Test {
public:
	int variable = 0;

	// CHECK: :[[@LINE+3]]
	// CHECK-NEXT: int function()
	// CHECK-NEXT: int function(bool u)
	int function() {
		return 1;
	}
};

// CHECK-NOT: :[[@LINE+1]]
int function() {
	return 1;
}

int main() {
	Test test = Test();

	// CHECK-NOT: :[[@LINE+1]]
	function();

	// CHECK: :[[@LINE+3]]
	// CHECK-NEXT: test.function()
	// CHECK-NEXT: test.function(true)
	return test.function();
}