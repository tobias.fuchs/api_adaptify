// RUN: api_refactoring changes.json "%s" -- | FileCheck-14 "%s"

typedef int MyInt;

// CHECK: :[[@LINE+3]]
// CHECK-NEXT: void foo(int bar)
// CHECK-NEXT: void foo(bool baz)
void foo(int bar) {
	// CHECK: :[[@LINE+3]]
	// CHECK-NEXT: foo(bar)
	// CHECK-NEXT: foo(true)
	foo(bar);
}

// CHECK: :[[@LINE+3]]
// CHECK-NEXT: const volatile MyInt test(int a, signed int b, unsigned int c)
// CHECK-NEXT: const volatile float test(int x, bool u, signed int b, unsigned int c)
const volatile MyInt test(int a, signed int b, unsigned int c);

// CHECK: :[[@LINE+3]]
// CHECK-NEXT: const volatile int test(int a, signed int b = 0, unsigned int c = 1)
// CHECK-NEXT: const volatile float test(int x, bool u, signed int b = 0, unsigned int c = 1)
const volatile int test(int a, signed int b = 0, unsigned int c = 1) {
	// CHECK: :[[@LINE+3]]
	// CHECK-NEXT: test(1, 2, 3)
	// CHECK-NEXT: test(1, true, 2, 3)
	return test(1, 2, 3);
}

// CHECK: :[[@LINE+3]]
// CHECK-NEXT: int main()
// CHECK-NEXT: int main(int argc, char** argv)
int main() {
	// CHECK: :[[@LINE+3]]
	// CHECK-NEXT: main()
	// CHECK-NEXT: main(0, nullptr)
	return main();
}