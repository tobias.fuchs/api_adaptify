// RUN: api_refactoring changes.json "%s" -- | FileCheck-14 "%s"

// CHECK: :[[@LINE+3]]
// CHECK-NEXT: T specialization()
// CHECK-NEXT: T templateSpecialization()
template <typename T> T specialization(T param);

// CHECK: :[[@LINE+3]]
// CHECK-NEXT: template <> int specialization<int>()
// CHECK-NEXT: template <> int templateSpecialization<int>()
template <> int specialization<int>(int param) {
	return param;
}

int main() {
	// CHECK: :[[@LINE+3]]
	// CHECK-NEXT: specialization<int>()
	// CHECK-NEXT: templateSpecialization<int>()
	specialization<int>(0);
}