#pragma once

#include <string>
#include <unordered_map>

#include <clang/AST/Decl.h>
#include <clang/AST/Expr.h>
#include <clang/AST/ASTContext.h>
#include <clang/Rewrite/Core/Rewriter.h>

#include "../JSON/Function.h"
#include "../Rewriter/RewriterVisitor.h"

class APIRefactoringVisitor : public RewriterVisitor<APIRefactoringVisitor> {
private:
	const std::unordered_map<std::string, Function>& functions;

	std::string getIdentifierFunctionDecl(clang::FunctionDecl* functionDecl);
	void applyInsertActionFunctionDecl(clang::FunctionDecl* functionDecl, const InsertAction& insertAction);
	void applyRemoveActionFunctionDecl(clang::FunctionDecl* functionDecl, const RemoveAction& removeAction);
	void applyReplaceActionFunctionDecl(clang::FunctionDecl* functionDecl, const ReplaceAction& replaceAction);

	std::string getIdentifierCallExpr(clang::CallExpr* callExpr);
	void applyInsertActionCallExpr(clang::CallExpr* callExpr, const InsertAction& insertAction);
	void applyRemoveActionCallExpr(clang::CallExpr* callExpr, const RemoveAction& removeAction);
	void applyReplaceActionCallExpr(clang::CallExpr* callExpr, const ReplaceAction& replaceAction);

public:
	explicit APIRefactoringVisitor(const clang::ASTContext& context, clang::Rewriter& rewriter, const std::unordered_map<std::string, Function>& functions);

	bool VisitFunctionDecl(clang::FunctionDecl* functionDecl);
	bool VisitCallExpr(clang::CallExpr* callExpr);
};