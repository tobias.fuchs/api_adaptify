#pragma once

#include <string>
#include <unordered_map>

#include <clang/AST/ASTContext.h>
#include <clang/Rewrite/Core/Rewriter.h>

#include "../JSON/Function.h"
#include "../Rewriter/RewriterConsumer.h"

#include "APIRefactoringVisitor.h"

class APIRefactoringConsumer : public RewriterConsumer<APIRefactoringVisitor, const std::unordered_map<std::string, Function>&> {
public:
	explicit APIRefactoringConsumer(const clang::ASTContext& context, clang::Rewriter& rewriter, const std::unordered_map<std::string, Function>& functions);
};