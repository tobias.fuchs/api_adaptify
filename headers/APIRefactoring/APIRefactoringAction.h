#pragma once

#include <string>
#include <unordered_map>

#include <llvm/ADT/StringRef.h>
#include <clang/AST/ASTConsumer.h>
#include <clang/Frontend/CompilerInstance.h>

#include "../JSON/Function.h"
#include "../Rewriter/RewriterAction.h"
#include "../Rewriter/RewriterConsumer.h"
#include "../Rewriter/RewriterVisitor.h"

#include "APIRefactoringVisitor.h"

class APIRefactoringAction : public RewriterAction<RewriterConsumer<RewriterVisitor<APIRefactoringVisitor>>> {
private:
	const std::unordered_map<std::string, Function>& functions;

public:
	explicit APIRefactoringAction(bool rewriteFiles, const std::unordered_map<std::string, Function>& functions);
	std::unique_ptr<clang::ASTConsumer> CreateASTConsumer(clang::CompilerInstance& compiler, llvm::StringRef inFile) override;
};