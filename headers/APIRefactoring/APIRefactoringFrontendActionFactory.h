#pragma once

#include <string>
#include <unordered_map>

#include <clang/Tooling/Tooling.h>
#include <clang/Frontend/FrontendAction.h>

#include "../JSON/Function.h"
#include "../Rewriter/RewriterFrontendActionFactory.h"
#include "../Rewriter/RewriterAction.h"
#include "../Rewriter/RewriterConsumer.h"
#include "../Rewriter/RewriterVisitor.h"

#include "APIRefactoringVisitor.h"

class APIRefactoringFrontendActionFactory : public RewriterFrontendActionFactory<RewriterAction<RewriterConsumer<RewriterVisitor<APIRefactoringVisitor>>>> {
private:
	const std::unordered_map<std::string, Function>& functions;

public:
	explicit APIRefactoringFrontendActionFactory(bool rewriteFiles, const std::unordered_map<std::string, Function>& functions);
	std::unique_ptr<clang::FrontendAction> create() override;
};

std::unique_ptr<clang::tooling::FrontendActionFactory> newAPIRefactoringFrontendActionFactory(bool rewriteFiles, const std::unordered_map<std::string, Function>& functions);