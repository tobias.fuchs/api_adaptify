#pragma once

#include <string>
#include <vector>

std::string identifier(const std::string& name, const std::vector<std::string>& arguments);