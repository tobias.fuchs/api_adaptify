#pragma once

#include <string>

#include <nlohmann/json.hpp>

#include "Target.h"

struct RemoveAction {
	Target::Target target;
	std::string value;
};

void to_json(nlohmann::json& j, const RemoveAction& p);
void from_json(const nlohmann::json& j, RemoveAction& p);