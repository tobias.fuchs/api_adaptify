#pragma once

#include <string>

#include <nlohmann/json.hpp>

#include "Target.h"
#include "Location.h"

struct InsertAction {
	Target::Target target;
	Location::Location insertionLocation;
	std::string reference;
	std::string value;
	std::string defaultValue;
};

void to_json(nlohmann::json& j, const InsertAction& p);
void from_json(const nlohmann::json& j, InsertAction& p);