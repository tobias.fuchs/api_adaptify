#pragma once

#include <string>

#include <llvm/Support/raw_ostream.h>
#include <nlohmann/json.hpp>

namespace Target {
	enum Target {
		Invalid,
		Function_Name,
		Parameter,
		Parameter_Name,
		Parameter_Type,
		Return_Type,
		Function_Qualifier,
		Function_Call
	};

	void to_json(nlohmann::json& j, const Target& target);
	void from_json(const nlohmann::json& j, Target& target);

	llvm::raw_ostream& operator<<(llvm::raw_ostream& os, const Target& target);
}