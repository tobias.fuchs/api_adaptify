#pragma once

#include <string>

#include <llvm/Support/raw_ostream.h>
#include <nlohmann/json.hpp>

namespace Location {
	enum Location {
		Invalid,
		Before,
		After
	};

	void to_json(nlohmann::json& j, const Location& location);
	void from_json(const nlohmann::json& j, Location& location);

	llvm::raw_ostream& operator<<(llvm::raw_ostream& os, const Location& location);
}