#pragma once

#include <string>
#include <vector>

#include <nlohmann/json.hpp>

#include "InsertAction.h"
#include "RemoveAction.h"
#include "ReplaceAction.h"

struct Function {
	std::string name;
	std::vector<std::string> arguments;
	std::vector<InsertAction> insertActions;
	std::vector<RemoveAction> removeActions;
	std::vector<ReplaceAction> replaceActions;
};

void to_json(nlohmann::json& j, const Function& p);
void from_json(const nlohmann::json& j, Function& p);