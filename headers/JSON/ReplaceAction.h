#pragma once

#include <string>

#include <nlohmann/json.hpp>

#include "Target.h"

struct ReplaceAction {
	Target::Target target;
	std::string oldValue;
	std::string newValue;
};

void to_json(nlohmann::json& j, const ReplaceAction& p);
void from_json(const nlohmann::json& j, ReplaceAction& p);