#pragma once

#include <string>
#include <vector>

#include <nlohmann/json.hpp>

#include "Function.h"

struct Change {
	std::string file;
	std::vector<Function> functions;
};

void to_json(nlohmann::json& j, const Change& p);
void from_json(const nlohmann::json& j, Change& p);