#pragma once

#include <clang/Tooling/Tooling.h>
#include <clang/Frontend/FrontendAction.h>

template <class T> class RewriterFrontendActionFactory : public clang::tooling::FrontendActionFactory {
protected:
	bool rewriteFiles;

public:
	explicit RewriterFrontendActionFactory(bool rewriteFiles) : rewriteFiles(rewriteFiles) {

	}

	std::unique_ptr<clang::FrontendAction> create() override {
		return std::make_unique<T>(rewriteFiles);
	}
};

template <class T> std::unique_ptr<clang::tooling::FrontendActionFactory> newRewriterFrontendActionFactory(bool rewriteFiles) {
	return std::unique_ptr<clang::tooling::FrontendActionFactory>(new RewriterFrontendActionFactory<T>(rewriteFiles));
}