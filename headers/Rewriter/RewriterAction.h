#pragma once

#include <clang/Rewrite/Core/Rewriter.h>
#include <clang/Frontend/FrontendAction.h>
#include <clang/Frontend/CompilerInstance.h>

#include "RewriterConsumer.h"
#include "RewriterVisitor.h"

template <class T> class RewriterAction : public clang::ASTFrontendAction {
protected:
	clang::Rewriter rewriter;
	bool rewriteFiles;

public:
	explicit RewriterAction(bool rewriteFiles) : rewriter(clang::Rewriter()), rewriteFiles(rewriteFiles) {

	}

	std::unique_ptr<clang::ASTConsumer> CreateASTConsumer(clang::CompilerInstance& compiler, llvm::StringRef inFile) override {
		rewriter.setSourceMgr(compiler.getSourceManager(), compiler.getLangOpts());
		return std::make_unique<T>(compiler.getASTContext(), rewriter);
	}

	void EndSourceFileAction() override {
		if (rewriteFiles) {
			rewriter.overwriteChangedFiles();
		}
	}
};