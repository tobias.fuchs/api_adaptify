#pragma once

#include <filesystem>

#include <clang/Lex/Lexer.h>

#include <clang/AST/ASTContext.h>
#include <clang/AST/RecursiveASTVisitor.h>
#include <clang/Rewrite/Core/Rewriter.h>

template <class T> class RewriterVisitor : public clang::RecursiveASTVisitor<T> {
private:
	bool print(const std::string& name, clang::SourceLocation sourceLocation) {
		std::string filepath = context.getSourceManager().getFilename(sourceLocation).str();
		std::string filename = std::filesystem::path(filepath).filename().string();

		unsigned int line = context.getSourceManager().getSpellingLineNumber(sourceLocation);
		unsigned int column = context.getSourceManager().getSpellingColumnNumber(sourceLocation);

		llvm::outs() << "FunctionDecl/" << name << ": ";
		llvm::outs() << filename << ":" << line << ":" << column << "\n";

		return true;
	}

	bool printChanges(clang::CharSourceRange printRange) {
		std::string currentSourceCode = clang::Lexer::getSourceText(printRange, context.getSourceManager(), context.getLangOpts()).str();
		std::string modifiedSourceCode = rewriter.getRewrittenText(printRange);

		if (currentSourceCode != modifiedSourceCode) {
			std::string filepath = context.getSourceManager().getFilename(printRange.getBegin()).str();
			std::string filename = std::filesystem::path(filepath).filename().string();

			unsigned int line = context.getSourceManager().getSpellingLineNumber(printRange.getBegin());

			llvm::outs() << filename << ":" << line << "\n";
			llvm::outs() << currentSourceCode << "\n";
			llvm::outs() << modifiedSourceCode << "\n";
			llvm::outs() << "\n";
		}

		return true;
	}

protected:
	const clang::ASTContext& context;
	clang::Rewriter& rewriter;

	virtual bool printFunctionDecl(clang::FunctionDecl* functionDecl) {
		return print(functionDecl->getNameAsString(), functionDecl->getLocation());
	}

	virtual bool printFunctionDeclChanges(clang::FunctionDecl* functionDecl) {
		clang::SourceRange sourceRange(functionDecl->getBeginLoc(), functionDecl->getTypeSpecEndLoc());
		clang::CharSourceRange charRange(sourceRange, true);
		return printChanges(charRange);
	}

	bool isFirst(clang::FunctionDecl* functionDecl, clang::SourceLocation location) {
		clang::SourceRange parameterRange(functionDecl->getFunctionTypeLoc().getLParenLoc(), functionDecl->getFunctionTypeLoc().getRParenLoc());

		if (!parameterRange.fullyContains(clang::SourceRange(location, location))) {
			llvm::errs() << "Location is not within parameterRange of function " << functionDecl->getQualifiedNameAsString() << "." << "\n";
			return false;
		}

		llvm::Optional<clang::Token> token = clang::Lexer::findNextToken(parameterRange.getBegin(), context.getSourceManager(), context.getLangOpts());

		clang::SourceRange sourceRange(token->getLocation(), location);
		clang::CharSourceRange charRange(sourceRange, false);

		std::string rewrittenText = rewriter.getRewrittenText(charRange);

		return std::all_of(rewrittenText.begin(), rewrittenText.end(), ::isspace);
	}

	bool isLast(clang::FunctionDecl* functionDecl, clang::SourceLocation location) {
		clang::SourceRange parameterRange(functionDecl->getFunctionTypeLoc().getLParenLoc(), functionDecl->getFunctionTypeLoc().getRParenLoc());

		if (!parameterRange.fullyContains(clang::SourceRange(location, location))) {
			llvm::errs() << "Location is not within parameterRange of function " << functionDecl->getQualifiedNameAsString() << "." << "\n";
			return false;
		}

		clang::SourceRange sourceRange(location, parameterRange.getEnd());
		clang::CharSourceRange charRange(sourceRange, false);

		std::string rewrittenText = rewriter.getRewrittenText(charRange);

		return std::all_of(rewrittenText.begin(), rewrittenText.end(), ::isspace);
	}

	bool replaceFunctionDeclName(clang::FunctionDecl* functionDecl, const std::string& name) {
		clang::SourceLocation sourceLocation = functionDecl->getLocation();
		llvm::Optional<clang::Token> token = clang::Lexer::findNextToken(sourceLocation, context.getSourceManager(), context.getLangOpts());
		clang::SourceRange sourceRange(functionDecl->getLocation(), token->getLocation());
		clang::CharSourceRange charRange(sourceRange, false);

		return rewriter.ReplaceText(charRange, name);
	}

	bool replaceFunctionDeclParamName(clang::FunctionDecl* functionDecl, unsigned int index, const std::string& name) {
		if (index >= functionDecl->getNumParams()) {
			return false;
		}

		clang::ParmVarDecl* paramDecl = functionDecl->getParamDecl(index);

		clang::SourceLocation location = paramDecl->getTypeSpecEndLoc();
		llvm::Optional<clang::Token> token = clang::Lexer::findNextToken(location, context.getSourceManager(), context.getLangOpts());

		clang::SourceRange sourceRange(token->getLocation(), paramDecl->getEndLoc());
		clang::CharSourceRange charRange(sourceRange, true);

		return rewriter.ReplaceText(charRange, name);
	}

	bool replaceFunctionDeclParamType(clang::FunctionDecl* functionDecl, unsigned int index, const std::string& type) {
		if (index >= functionDecl->getNumParams()) {
			return false;
		}

		clang::ParmVarDecl* paramDecl = functionDecl->getParamDecl(index);
		clang::SourceRange sourceRange(paramDecl->getTypeSpecStartLoc(), paramDecl->getTypeSpecEndLoc());
		clang::CharSourceRange charRange(sourceRange, true);
		return rewriter.ReplaceText(charRange, type);
	}

	clang::CharSourceRange indexToSourceRange(clang::FunctionDecl* functionDecl, unsigned int index) {
		clang::CharSourceRange charRange(functionDecl->getParamDecl(index)->getSourceRange(), true);
		return charRange;
	}

	bool removeFunctionDeclParam(clang::FunctionDecl* functionDecl, unsigned int index) {
		if (index >= functionDecl->getNumParams()) {
			return false;
		}

		clang::CharSourceRange range = indexToSourceRange(functionDecl, index);

		if (!isLast(functionDecl, clang::Lexer::findNextToken(range.getEnd(), context.getSourceManager(), context.getLangOpts())->getLocation())) {
			clang::ParmVarDecl* paramDecl = functionDecl->getParamDecl(index);
			llvm::Optional<clang::Token> comma = clang::Lexer::findNextToken(paramDecl->getEndLoc(), context.getSourceManager(), context.getLangOpts());
			llvm::Optional<clang::Token> token = clang::Lexer::findNextToken(comma->getLocation(), context.getSourceManager(), context.getLangOpts());

			clang::SourceRange sourceRange(range.getBegin(), token->getLocation());
			clang::CharSourceRange charRange = clang::CharSourceRange(sourceRange, false);

			return rewriter.RemoveText(charRange);
		}
		else {
			if (isFirst(functionDecl, range.getBegin())) {
				return rewriter.RemoveText(range);
			}
			else {
				clang::ParmVarDecl* paramDecl = functionDecl->getParamDecl(index - 1);
				llvm::Optional<clang::Token> comma = clang::Lexer::findNextToken(paramDecl->getEndLoc(), context.getSourceManager(), context.getLangOpts());

				clang::SourceRange sourceRange(comma->getLocation(), range.getEnd());
				clang::CharSourceRange charRange = clang::CharSourceRange(sourceRange, true);

				return rewriter.RemoveText(charRange);
			}
		}
	}

	clang::SourceLocation indexToSourceLocation(clang::FunctionDecl* functionDecl, unsigned int index) {
		if (functionDecl->getNumParams() == 0) {
			return functionDecl->getTypeSpecEndLoc();
		}

		if (index < functionDecl->getNumParams()) {
			return functionDecl->getParamDecl(index)->getBeginLoc();
		}
		else {
			clang::SourceLocation location = functionDecl->getParamDecl(functionDecl->getNumParams() - 1)->getEndLoc();
			llvm::Optional<clang::Token> token = clang::Lexer::findNextToken(location, context.getSourceManager(), context.getLangOpts());
			return token->getLocation();
		}
	}

	bool insertFunctionDeclParam(clang::FunctionDecl* functionDecl, unsigned int index, const std::string& param) {
		if (index > functionDecl->getNumParams()) {
			return false;
		}

		clang::SourceLocation location = indexToSourceLocation(functionDecl, index);

		if (!isLast(functionDecl, location)) {
			return rewriter.InsertTextBefore(location, param + ", ");
		}
		else {
			if (isFirst(functionDecl, location)) {
				return rewriter.InsertText(location, param);
			}
			else {
				return rewriter.InsertTextAfter(location, ", " + param);
			}
		}
	}

	virtual bool printCallExpr(clang::CallExpr* callExpr) {
		return print(callExpr->getDirectCallee()->getNameAsString(), callExpr->getExprLoc());
	}

	virtual bool printCallExprChanges(clang::CallExpr* callExpr) {
		clang::SourceRange sourceRange = callExpr->getSourceRange();
		clang::CharSourceRange charRange(sourceRange, true);
		return printChanges(charRange);
	}

	bool isFirst(clang::CallExpr* callExpr, clang::SourceLocation location) {
		llvm::Optional<clang::Token> currentToken = clang::Lexer::findNextToken(callExpr->getExprLoc(), context.getSourceManager(), context.getLangOpts());

		while (std::strcmp(currentToken->getName(), "l_paren") != 0) {
			currentToken = clang::Lexer::findNextToken(currentToken->getEndLoc(), context.getSourceManager(), context.getLangOpts());
		}

		clang::SourceRange parameterRange(currentToken->getEndLoc(), callExpr->getRParenLoc());

		if (!parameterRange.fullyContains(clang::SourceRange(location, location))) {
			llvm::errs() << "Location is not within parameterRange of function " << callExpr->getDirectCallee()->getQualifiedNameAsString() << "." << "\n";
			return false;
		}

		clang::SourceRange sourceRange(parameterRange.getBegin(), location);
		clang::CharSourceRange charRange(sourceRange, false);

		std::string rewrittenText = rewriter.getRewrittenText(charRange);

		return std::all_of(rewrittenText.begin(), rewrittenText.end(), ::isspace);
	}

	bool isLast(clang::CallExpr* callExpr, clang::SourceLocation location) {
		llvm::Optional<clang::Token> currentToken = clang::Lexer::findNextToken(callExpr->getExprLoc(), context.getSourceManager(), context.getLangOpts());

		while (std::strcmp(currentToken->getName(), "l_paren") != 0) {
			currentToken = clang::Lexer::findNextToken(currentToken->getEndLoc(), context.getSourceManager(), context.getLangOpts());
		}

		clang::SourceRange parameterRange(currentToken->getEndLoc(), callExpr->getRParenLoc());

		if (!parameterRange.fullyContains(clang::SourceRange(location, location))) {
			llvm::errs() << "Location is not within parameterRange of function " << callExpr->getDirectCallee()->getQualifiedNameAsString() << "." << "\n";
			return false;
		}

		clang::SourceRange sourceRange(location, parameterRange.getEnd());
		clang::CharSourceRange charRange(sourceRange, false);

		std::string rewrittenText = rewriter.getRewrittenText(charRange);

		return std::all_of(rewrittenText.begin(), rewrittenText.end(), ::isspace);
	}

	bool replaceCallExprName(clang::CallExpr* callExpr, const std::string& name) {
		clang::SourceLocation sourceLocation = callExpr->getBeginLoc();
		llvm::Optional<clang::Token> token = clang::Lexer::findNextToken(sourceLocation, context.getSourceManager(), context.getLangOpts());
		clang::SourceRange sourceRange(callExpr->getBeginLoc(), token->getLocation());
		clang::CharSourceRange charRange(sourceRange, false);

		return rewriter.ReplaceText(charRange, name);
	}

	bool replaceCallExprParam(clang::CallExpr* callExpr, unsigned int index, const std::string& param) {
		if (index >= callExpr->getNumArgs()) {
			return false;
		}

		clang::Expr* argExpr = callExpr->getArg(index);
		clang::SourceRange sourceRange = argExpr->getSourceRange();
		clang::CharSourceRange charRange(sourceRange, true);
		return rewriter.ReplaceText(charRange, param);
	}

	clang::CharSourceRange indexToSourceRange(clang::CallExpr* callExpr, unsigned int index) {
		clang::CharSourceRange charRange(callExpr->getArg(index)->getSourceRange(), true);
		return charRange;
	}

	bool removeCallExprParam(clang::CallExpr* callExpr, unsigned int index) {
		if (index >= callExpr->getNumArgs()) {
			return false;
		}

		clang::CharSourceRange range = indexToSourceRange(callExpr, index);

		if (!isLast(callExpr, clang::Lexer::findNextToken(range.getEnd(), context.getSourceManager(), context.getLangOpts())->getLocation())) {
			clang::Expr* argExpr = callExpr->getArg(index);
			llvm::Optional<clang::Token> comma = clang::Lexer::findNextToken(argExpr->getEndLoc(), context.getSourceManager(), context.getLangOpts());
			llvm::Optional<clang::Token> token = clang::Lexer::findNextToken(comma->getLocation(), context.getSourceManager(), context.getLangOpts());

			clang::SourceRange sourceRange(range.getBegin(), token->getLocation());
			clang::CharSourceRange charRange = clang::CharSourceRange(sourceRange, false);

			return rewriter.RemoveText(charRange);
		}
		else {
			if (isFirst(callExpr, range.getBegin())) {
				return rewriter.RemoveText(range);
			}
			else {
				clang::Expr* argExpr = callExpr->getArg(index - 1);
				llvm::Optional<clang::Token> comma = clang::Lexer::findNextToken(argExpr->getEndLoc(), context.getSourceManager(), context.getLangOpts());

				clang::SourceRange sourceRange(comma->getLocation(), range.getEnd());
				clang::CharSourceRange charRange = clang::CharSourceRange(sourceRange, true);

				return rewriter.RemoveText(charRange);
			}
		}
	}

	clang::SourceLocation indexToSourceLocation(clang::CallExpr* callExpr, unsigned int index) {
		if (callExpr->getNumArgs() == 0) {
			return callExpr->getEndLoc();
		}

		if (index < callExpr->getNumArgs()) {
			return callExpr->getArg(index)->getBeginLoc();
		}
		else {
			clang::SourceLocation location = callExpr->getArg(callExpr->getNumArgs() - 1)->getEndLoc();
			llvm::Optional<clang::Token> token = clang::Lexer::findNextToken(location, context.getSourceManager(), context.getLangOpts());
			return token->getLocation();
		}
	}

	bool insertCallExprParam(clang::CallExpr* callExpr, unsigned int index, const std::string& param) {
		if (index > callExpr->getNumArgs()) {
			return false;
		}

		clang::SourceLocation location = indexToSourceLocation(callExpr, index);

		if (!isLast(callExpr, location)) {
			return rewriter.InsertTextBefore(location, param + ", ");
		}
		else {
			if (isFirst(callExpr, location)) {
				return rewriter.InsertText(location, param);
			}
			else {
				return rewriter.InsertTextAfter(location, ", " + param);
			}
		}
	}

	bool replaceReturnType(clang::FunctionDecl* functionDecl, const std::string& type) {
		clang::SourceRange sourceRange = functionDecl->getReturnTypeSourceRange();
		clang::CharSourceRange charRange(sourceRange, true);
		return rewriter.ReplaceText(charRange, type);
	}

	bool replaceQualifier(clang::FunctionDecl* functionDecl, const std::string& qualifier) {
		clang::SourceRange sourceRange(functionDecl->getBeginLoc(), functionDecl->getReturnTypeSourceRange().getBegin());

		if (functionDecl->isFunctionTemplateSpecialization()) {
			sourceRange.setBegin(functionDecl->getInnerLocStart());
		}

		if (clang::dyn_cast<clang::CXXConstructorDecl>(functionDecl) != nullptr) {
			sourceRange.setEnd(functionDecl->getLocation());
		}

		clang::CharSourceRange charRange(sourceRange, false);
		return rewriter.ReplaceText(charRange, qualifier + " ");
	}

public:
	explicit RewriterVisitor(const clang::ASTContext& context, clang::Rewriter& rewriter) : context(context), rewriter(rewriter) {

	}
};