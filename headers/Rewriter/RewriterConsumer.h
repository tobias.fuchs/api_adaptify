#pragma once

#include <clang/AST/ASTContext.h>
#include <clang/AST/ASTConsumer.h>
#include <clang/Rewrite/Core/Rewriter.h>

#include "RewriterVisitor.h"

template <class T, class ... Args> class RewriterConsumer : public clang::ASTConsumer {
private:
	T visitor;

public:
	explicit RewriterConsumer(const clang::ASTContext& context, clang::Rewriter& rewriter, Args ... args) : visitor(context, rewriter, args...) {

	}

	void HandleTranslationUnit(clang::ASTContext& context) override {
		visitor.TraverseAST(context);
	}
};