FROM ubuntu:22.04

WORKDIR /opt

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update -y
RUN apt-get upgrade -y

RUN apt-get install -y software-properties-common
RUN add-apt-repository universe

RUN apt-get install -y gcc g++ gdb cmake cmake-curses-gui python3 apt-utils wget gnupg git autoconf automake libtool zlib1g-dev zlib1g vim unzip python3-pip python3-pytest python3-pytest-cov openmpi-bin openmpi-common bison flex

RUN apt-get install -y libllvm-14-ocaml-dev libllvm14 llvm-14 llvm-14-dev llvm-14-doc llvm-14-examples llvm-14-runtime clang-14 clang-tools-14 clang-14-doc libclang-common-14-dev libclang-14-dev libclang1-14 clang-format-14 python3-clang-14 clangd-14 clang-tidy-14

RUN apt-get install -y nlohmann-json3-dev

RUN ln -s /usr/bin/clang-14 /usr/bin/clang && ln -s /usr/bin/clang++-14 /usr/bin/clang++ && ln -s /usr/lib/llvm-14/build/utils/lit/lit.py /usr/bin/lit && ln -s /usr/bin/opt-14 /usr/bin/opt && ln -s /usr/bin/FileCheck-14 /usr/bin/FileCheck

RUN apt-get update && apt-get install -y ssh rsync tar

RUN ( \
    echo 'LogLevel DEBUG2'; \
    echo 'PermitRootLogin yes'; \
    echo 'PasswordAuthentication yes'; \
    echo 'Subsystem sftp /usr/lib/openssh/sftp-server'; \
  ) > /etc/ssh/sshd_config_test_clion \
  && mkdir /run/sshd

RUN useradd -m user \
  && yes password | passwd user

RUN usermod -s /bin/bash user

CMD ["/usr/sbin/sshd", "-D", "-e", "-f", "/etc/ssh/sshd_config_test_clion"]
